import Adapter.BreadMakerMachine;
import Builder.BlackBreadBake;
import Builder.Director;
import Builder.Recipe;
import Builder.WhiteBreadBake;
import Memento.RecipeHistory;
import Memento.RecipeMemento;

import java.util.Scanner;

public class Main {
    private static RecipeHistory recipeHistory;
    private static Director director = new Director(new BreadMakerMachine());
    private static void saveRecipe(Recipe recipe) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Хотите сохранить рецепт?");
        System.out.println("1. Да");
        System.out.println("2. Нет");
        if (scanner.nextInt() == 1) {
            director.saveRecipe(recipe);
        }
    }

    private static void showMenu() {
        System.out.println("1. Приготовить белый хлеб");
        System.out.println("2. Приготовить черный хлеб");
        System.out.println("3. Приготовить хлеб по собственному рецепту");
        System.out.println("4. Приготовить хлеб по сохраненному рецепту");
        System.out.print("Введите номер опции, которую хотите выбрать \n➤ ");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        recipeHistory = new RecipeHistory();

        while (true) {
            showMenu();
            switch (scanner.nextInt()) {
                case(1):
                    WhiteBreadBake whiteBreadBake = new WhiteBreadBake();
                    Director director1 = new Director(whiteBreadBake);
                    director1.doBread();
                    System.out.println(whiteBreadBake.getProduct());
                    break;
                case (2):
                    BlackBreadBake blackBreadBake = new BlackBreadBake();
                    Director director2 = new Director(blackBreadBake);
                    director2.doBread();
                    System.out.println(blackBreadBake.getProduct());
                    break;
                case (3):
                    BreadMakerMachine breadMakerMachine1 = new BreadMakerMachine();
                    Director director3 = new Director(breadMakerMachine1);
                    director3.doBread();
                    System.out.println(breadMakerMachine1.getProduct());
                    //Recipe rec = director.doBread();
                    System.out.println(breadMakerMachine1.getProduct());
                    saveRecipe(breadMakerMachine1.getProduct());
                    break;
                case (4):
                    director.recipeHistory.showHistory();
                    System.out.print("Выберите рецепт \n ➤ ");
                    RecipeMemento recipe = director.recipeHistory.history.get(scanner.nextInt());
                    BreadMakerMachine breadMakerMachine = new BreadMakerMachine(recipe.getWheatFlour(),
                            recipe.getRyeFlour(), recipe.getDryYeast(), recipe.getMalt(), recipe.getSalt(),
                            recipe.getSugar(), recipe.getWater(), recipe.getVegetableOil(), recipe.getMargarine());
                    Director director4 = new Director(breadMakerMachine);
                    director4.doBreadSaved();
                    System.out.println(breadMakerMachine.getProduct());
                    break;
                default:
                    System.out.println("Неверное число!");
            }
        }
    }
}