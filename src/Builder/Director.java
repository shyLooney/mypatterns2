package Builder;

import Memento.RecipeHistory;
import Memento.RecipeMemento;

public class Director {
    private Baker baker;
    public RecipeHistory recipeHistory;

    public Director(Baker baker) {
        this.baker = baker;
        recipeHistory = new RecipeHistory();
    }

    public void doBread() {
        baker.setWheatFlour()
                .setRyeFlour()
                .setDryYeast()
                .setMalt()
                .setSalt()
                .setSugar()
                .setWater()
                .setVegetableOil()
                .setMargarine().build();
    }

    public void doBreadSaved() {
        baker.build();
    }

    public void saveRecipe(Recipe recipe) {
        recipeHistory.history.add(new RecipeMemento(recipe.getWheatFlour(),
                recipe.getRyeFlour(), recipe.getDryYeast(), recipe.getMalt(), recipe.getSalt(),
                recipe.getSugar(), recipe.getWater(), recipe.getVegetableOil(), recipe.getMargarine()));
    }
}
