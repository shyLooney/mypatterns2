package Builder;

public interface Baker {
    Baker setWheatFlour();
    Baker setRyeFlour();
    Baker setDryYeast();
    Baker setMalt();
    Baker setSalt();
    Baker setSugar();
    Baker setWater();
    Baker setVegetableOil();
    Baker setMargarine();
    Baker build();
}
