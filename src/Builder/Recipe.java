package Builder;

public abstract class Recipe {
    protected int wheatFlour;
    protected int ryeFlour;
    protected int dryYeast;
    protected int malt;
    protected int salt;
    protected int sugar;
    protected int water;
    protected int vegetableOil;
    protected int margarine;

    public Recipe(int wheatFlour, int ryeFlour, int dryYeast, int malt,
                  int salt, int sugar, int water, int vegetableOil, int margarine) {
        this.wheatFlour = wheatFlour;
        this.ryeFlour = ryeFlour;
        this.dryYeast = dryYeast;
        this.malt = malt;
        this.salt = salt;
        this.sugar = sugar;
        this.water = water;
        this.vegetableOil = vegetableOil;
        this.margarine = margarine;
    }


    public int getWheatFlour() {
        return wheatFlour;
    }

    public void setWheatFlour(int wheatFlour) {
        this.wheatFlour = wheatFlour;
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public int getMargarine() {
        return margarine;
    }

    public void setMargarine(int margarine) {
        this.margarine = margarine;
    }

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    public int getSalt() {
        return salt;
    }

    public void setSalt(int salt) {
        this.salt = salt;
    }

    public int getDryYeast() {
        return dryYeast;
    }

    public void setDryYeast(int dryYeast) {
        this.dryYeast = dryYeast;
    }

    public int getRyeFlour() {
        return ryeFlour;
    }

    public void setRyeFlour(int ryeFlour) {
        this.ryeFlour = ryeFlour;
    }

    public int getMalt() {
        return malt;
    }

    public void setMalt(int malt) {
        this.malt = malt;
    }

    public int getVegetableOil() {
        return vegetableOil;
    }

    public void setVegetableOil(int vegetableOil) {
        this.vegetableOil = vegetableOil;
    }
}
