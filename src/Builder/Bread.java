package Builder;

public class Bread extends Recipe {

    public Bread(int wheatFlour, int ryeFlour, int dryYeast, int malt,
                 int salt, int sugar, int water, int vegetableOil, int margarine) {
        super(wheatFlour, ryeFlour, dryYeast, malt, salt, sugar, water, vegetableOil, margarine);
    }

    @Override
    public String toString() {
        return "Хлеб состоит из:\n" +
                "\tПшеничной муки " + wheatFlour + " гр.\n" +
                "\tРжаной муки " + ryeFlour + " гр.\n" +
                "\tСухих дрожжей " + dryYeast + " гр.\n" +
                "\tСолода " + malt + " гр.\n" +
                "\tСоли " + salt + " гр.\n" +
                "\tСахара " + sugar + " гр.\n" +
                "\tВоды " + water + " мл.\n" +
                "\tРастительного масла " + vegetableOil + " мл.\n" +
                "\tМаргарина " + margarine + " гр.\n";
    }

}
