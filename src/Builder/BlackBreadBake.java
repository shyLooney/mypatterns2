package Builder;

public class BlackBreadBake implements Baker {
    private int wheatFlour;
    private int ryeFlour;
    private int dryYeast;
    private int malt;
    private int salt;
    private int sugar;
    private int water;
    private int vegetableOil;
    private int margarine;
    private Bread bread;

    public BlackBreadBake() {
        System.out.println("Подготовка ингредиентов для приготовления черного хлеба пекарем!");
    }

    @Override
    public Baker setWheatFlour() {
        System.out.println("\tКоличество пшеничной муки установлено на 380 гр.");
        this.wheatFlour = 380;
        return this;
    }

    @Override
    public Baker setRyeFlour() {
        System.out.println("\tКоличество ржаной муки установлено на 350 гр.");
        this.ryeFlour = 350;
        return this;
    }

    @Override
    public Baker setDryYeast() {
        System.out.println("\tКоличество сухих дрожжей установлено на 10 гр.");
        this.dryYeast = 10;
        return this;
    }

    @Override
    public Baker setMalt() {
        System.out.println("\tКоличество солода установлено на 96 гр.");
        this.malt = 96;
        return this;
    }

    @Override
    public Baker setSalt() {
        System.out.println("\tКоличество соли установлено на 8 гр.");
        this.salt = 8;
        return this;
    }

    @Override
    public Baker setSugar() {
        System.out.println("\tКоличество сахара установлено на 20 гр.");
        this.sugar = 20;
        return this;
    }

    @Override
    public Baker setWater() {
        System.out.println("\tКоличество воды установлено на 500 мл.");
        this.water = 500;
        return this;
    }

    @Override
    public Baker setVegetableOil() {
        System.out.println("\tКоличество растительного масла установлено на 20 мл.");
        this.vegetableOil = 20;
        return this;
    }

    @Override
    public Baker setMargarine() {
        System.out.println("\tМаргарин не добавлен.");
        this.margarine = 0;
        return this;
    }

    public Baker build() {
        bread = new Bread(wheatFlour, ryeFlour, dryYeast, malt, salt,
                sugar, water, vegetableOil, margarine);
        System.out.println("Черный хлеб был приготовлен!\n");
        return this;
    }

    public Bread getProduct() {
        return bread;
    }
}
