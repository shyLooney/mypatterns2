package Builder;

public class WhiteBreadBake implements Baker {
    private int wheatFlour;
    private int ryeFlour;
    private int dryYeast;
    private int malt;
    private int salt;
    private int sugar;
    private int water;
    private int vegetableOil;
    private int margarine;
    private Bread bread;

    public WhiteBreadBake() {
        System.out.println("Подготовка ингредиентов для приготовления белого хлеба пекарем!");
    }

    @Override
    public Baker setWheatFlour() {
        System.out.println("\tКоличество пшеничной муки установлено на 500 гр.");
        this.wheatFlour = 500;
        return this;
    }

    @Override
    public Baker setRyeFlour() {
        System.out.println("\tРжаная мука не добавлена.");
        this.ryeFlour = 0;
        return this;
    }

    @Override
    public Baker setDryYeast() {
        System.out.println("\tКоличество сухих дрожжей установлено на 2 гр.");
        this.dryYeast = 2;
        return this;
    }

    @Override
    public Baker setMalt() {
        System.out.println("\tСолод не добавлен.");
        this.malt = 0;
        return this;
    }

    @Override
    public Baker setSalt() {
        System.out.println("\tКоличество соли установлено на 7 гр.");
        this.salt = 7;
        return this;
    }

    @Override
    public Baker setSugar() {
        System.out.println("\tКоличество сахара установлено на 20 гр.");
        this.sugar = 20;
        return this;
    }

    @Override
    public Baker setWater() {
        System.out.println("\tКоличество воды установлено на 270 мл.");
        this.water = 270;
        return this;
    }

    @Override
    public Baker setVegetableOil() {
        System.out.println("\tРастительное масло не добавлено.");
        this.vegetableOil = 0;
        return this;
    }

    @Override
    public Baker setMargarine() {
        System.out.println("\tКоличество маргарина установлено на 20 гр.");
        this.margarine = 20;
        return this;
    }

    public Baker build() {
        bread = new Bread(wheatFlour, ryeFlour, dryYeast, malt, salt,
                sugar, water, vegetableOil, margarine);
        System.out.println("Белый хлеб был приготовлен!\n");
        return this;
    }

    public Bread getProduct() {
        return bread;
    }
}
