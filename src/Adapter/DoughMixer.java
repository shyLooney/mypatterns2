package Adapter;

import java.util.concurrent.TimeUnit;

public class DoughMixer {
    private HandmadeBread dough;

    public DoughMixer (int wheatFlour, int ryeFlour, int dryYeast, int malt,
                 int salt, int sugar, int water, int vegetableOil, int margarine) {
        dough = new HandmadeBread(wheatFlour, ryeFlour, dryYeast, malt,
                salt, sugar, water, vegetableOil, margarine);
        knead();
        infuse();
    }
    public void knead() {
        System.out.println("Тесто было замешано");
    }

    public void infuse() {
        for (int i = 0; i < 3; i++) {
            System.out.println("Тесто настаивается...");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        dough.setFlag(true);
        System.out.println("Тесто настоялось!\n");
    }

    public HandmadeBread getDough() {
        return dough;
    }
}
