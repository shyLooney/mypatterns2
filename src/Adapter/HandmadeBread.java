package Adapter;

import Builder.Recipe;

public class HandmadeBread extends Recipe {
    private boolean flag;

    public HandmadeBread(int wheatFlour, int ryeFlour, int dryYeast, int malt,
                         int salt, int sugar, int water, int vegetableOil, int margarine) {
        super(wheatFlour, ryeFlour, dryYeast, malt, salt, sugar, water, vegetableOil, margarine);
    }

    @Override
    public String toString() {
        if (flag) {
            return "Хлеб готов и состоит из:\n" +
                    "\tПшеничной муки " + wheatFlour + " гр.\n" +
                    "\tРжаной муки " + ryeFlour + " гр.\n" +
                    "\tСухих дрожжей " + dryYeast + " гр.\n" +
                    "\tСолода " + malt + " гр.\n" +
                    "\tСоли " + salt + " гр.\n" +
                    "\tСахара " + sugar + " гр.\n" +
                    "\tВоды " + water + " мл.\n" +
                    "\tРастительного масла " + vegetableOil + " мл.\n" +
                    "\tМаргарина " + margarine + " гр.";
        }
        return "Тесто не готово и состоит из:\n" +
                "\tПшеничной муки " + wheatFlour + " гр.\n" +
                "\tРжаной муки " + ryeFlour + " гр.\n" +
                "\tСухих дрожжей " + dryYeast + " гр.\n" +
                "\tСолода " + malt + " гр.\n" +
                "\tСоли " + salt + " гр.\n" +
                "\tСахара " + sugar + " гр.\n" +
                "\tВоды " + water + " мл.\n" +
                "\tРастительного масла " + vegetableOil + " мл.\n" +
                "\tМаргарина " + margarine + " гр.\n";
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
