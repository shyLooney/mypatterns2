package Adapter;

import Builder.Baker;
import Builder.Bread;
import Builder.Recipe;
import Memento.RecipeHistory;
import Memento.RecipeMemento;

import java.util.Scanner;

public class BreadMakerMachine implements Baker {
    private int wheatFlour;
    private int ryeFlour;
    private int dryYeast;
    private int malt;
    private int salt;
    private int sugar;
    private int water;
    private int vegetableOil;
    private int margarine;
    public RecipeHistory recipeHistory;
    private DoughMixer doughMixer;
    Scanner scanner;

    public BreadMakerMachine(int wheatFlour, int ryeFlour, int dryYeast, int malt,
                             int salt, int sugar, int water, int vegetableOil, int margarine) {
        this.wheatFlour = wheatFlour;
        this.ryeFlour = ryeFlour;
        this.dryYeast = dryYeast;
        this.malt = malt;
        this.salt = salt;
        this.sugar = sugar;
        this.water = water;
        this.vegetableOil = vegetableOil;
        this.margarine = margarine;
        scanner = new Scanner(System.in);
        recipeHistory = new RecipeHistory();
        System.out.println("Подготовка теста для приготовления хлеба в хлебопечки!");
    }

    public BreadMakerMachine() {
        scanner = new Scanner(System.in);
        System.out.println("Подготовка теста для приготовления хлеба в хлебопечки!");
    }

    @Override
    public Baker setWheatFlour() {
        System.out.print("\tВведите количество гр. пшеничной муки: ");
        this.wheatFlour = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setRyeFlour() {
        System.out.print("\tВведите количество гр. ржаной муки: ");
        this.ryeFlour = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setDryYeast() {
        System.out.print("\tВведите количество гр. сухих дрожжей: ");
        this.dryYeast = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setMalt() {
        System.out.print("\tВведите количество гр. солода: ");
        this.malt = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setSalt() {
        System.out.print("\tВведите количество гр. соли: ");
        this.salt = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setSugar() {
        System.out.print("\tВведите количество гр. сахара: ");
        this.sugar = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setWater() {
        System.out.print("\tВведите количество мл. воды: ");
        this.water = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setVegetableOil() {
        System.out.print("\tВведите количество мл. растительного: ");
        this.vegetableOil = scanner.nextInt();
        return this;
    }

    @Override
    public Baker setMargarine() {
        System.out.print("\tВведите количество гр. маргарина: ");
        this.margarine = scanner.nextInt();
        return this;
    }

    public Baker build() {
        doughMixer = new DoughMixer(wheatFlour, ryeFlour, dryYeast, malt,
                salt, sugar, water, vegetableOil, margarine);
        System.out.println("Хлебопечь приготовила хлеб из полученного теста!\n");
        return this;
    }

    public HandmadeBread doBreadSaved() {
        build();
        return getProduct();
    }

    public void saveRecipe(Recipe recipe) {
        recipeHistory.history.add(new RecipeMemento(recipe.getWheatFlour(),
                recipe.getRyeFlour(), recipe.getDryYeast(), recipe.getMalt(), recipe.getSalt(),
                recipe.getSugar(), recipe.getWater(), recipe.getVegetableOil(), recipe.getMargarine()));
    }

    public HandmadeBread getProduct() {
        return doughMixer.getDough();
    }
}
