package Memento;

import java.util.ArrayList;

public class RecipeHistory {
    public ArrayList<RecipeMemento> history;

    public RecipeHistory() {
        history = new ArrayList<>();
    }

    public void showHistory() {
        for (int i = 0; i < history.size(); i++) {
            System.out.println("Рецепт №" + i + ": " + history.get(i).toString());
        }
    }
}
