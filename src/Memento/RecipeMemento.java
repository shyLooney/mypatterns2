package Memento;


public class RecipeMemento {
    private int wheatFlour;
    private int ryeFlour;
    private int dryYeast;
    private int malt;
    private int salt;
    private int sugar;
    private int water;
    private int vegetableOil;
    private int margarine;

    public RecipeMemento(int wheatFlour, int ryeFlour, int dryYeast, int malt,
                  int salt, int sugar, int water, int vegetableOil, int margarine) {
        this.wheatFlour = wheatFlour;
        this.ryeFlour = ryeFlour;
        this.dryYeast = dryYeast;
        this.malt = malt;
        this.salt = salt;
        this.sugar = sugar;
        this.water = water;
        this.vegetableOil = vegetableOil;
        this.margarine = margarine;
    }

    @Override
    public String toString() {
        return "{" +
                "пшеничная мука гр. =" + wheatFlour +
                ", ржаная мука гр.=" + ryeFlour +
                ", сухие дрожжи гр.=" + dryYeast +
                ", солод гр.=" + malt +
                ", соль гр.=" + salt +
                ", сахар гр.=" + sugar +
                ", вода мл.=" + water +
                ", растительное масло мл.=" + vegetableOil +
                ", маргарин гр.=" + margarine +
                '}';
    }

    public int getWheatFlour() {
        return wheatFlour;
    }

    public int getRyeFlour() {
        return ryeFlour;
    }

    public int getDryYeast() {
        return dryYeast;
    }

    public int getMalt() {
        return malt;
    }

    public int getSalt() {
        return salt;
    }

    public int getSugar() {
        return sugar;
    }

    public int getWater() {
        return water;
    }

    public int getVegetableOil() {
        return vegetableOil;
    }

    public int getMargarine() {
        return margarine;
    }
}
