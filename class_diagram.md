```plantuml
@startuml абоба
scale 2
skinparam ClassAttributeIconSize 0

interface Baker
{
	Baker setWheatFlour()
	Baker setRyeFlour()
	Baker setDryYeast()
	Baker setMalt()
	Baker setSalt()
	Baker setSugar()
	Baker setWater()
	Baker setVegetableOil()
	Baker setMargarine()
	Recipe build()
}

class BlackBreadBake
{
	-Integer wheatFlour;
    -Integer ryeFlour;
    -Integer dryYeast;
    -Integer malt;
    -Integer salt;
    -Integer sugar;
    -Integer water;
    -Integer vegetableOil;
    -Integer margarine;
	-Bread bread;
	+Bread getProduct()
}

class WhiteBreadBake
{
	-Integer wheatFlour;
    -Integer ryeFlour;
    -Integer dryYeast;
    -Integer malt;
    -Integer salt;
    -Integer sugar;
    -Integer water;
    -Integer vegetableOil;
	-Bread bread;
    -Integer margarine;
	+Bread getProduct()
}

class BreadMakerMachine
{
	-Integer wheatFlour;
    -Integer ryeFlour;
    -Integer dryYeast;
    -Integer malt;
    -Integer salt;
    -Integer sugar;
    -Integer water;
    -Integer vegetableOil;
    -Integer margarine;
	-DoughMixer doughMixer;
	-Scanner scanner
	+HandMadeBread getProduct()
}

class Director
{
	-Baker baker
	+Director(Baker)
	+Recipe doBread()
	+Recipe doBreadSaved()
}

abstract class Recipe
{
	-Integer wheatFlour;
    -Integer ryeFlour;
    -Integer dryYeast;
    -Integer malt;
    -Integer salt;
    -Integer sugar;
    -Integer water;
    -Integer vegetableOil;
    -Integer margarine;
	+Recipe(Integer, Integer, Integer, Integer, Integer,
	Integer, Integer, Integer, Integer)
}

class Bread
{

}

class HandmadeBread
{
	-Boolean flag
	-setFlag(Boolean)
}

class DoughMixer
{
	-HandmadeBread dough
	-knead()
	-infuse()
}

class RecipeHistory
{
	-ArrayList<RecipeMemento> history
	+showHistory()
}

class RecipeMemento
{
	-Integer wheatFlour;
    -Integer ryeFlour;
    -Integer dryYeast;
    -Integer malt;
    -Integer salt;
    -Integer sugar;
    -Integer water;
    -Integer vegetableOil;
    -Integer margarine;
	+RecipeMemento(Integer, Integer, Integer, Integer, Integer,
	Integer, Integer, Integer, Integer)	
}

BlackBreadBake --|> Baker
WhiteBreadBake --|> Baker
BreadMakerMachine --|> Baker
Baker --o Director
Bread --|> Recipe
HandmadeBread --|> Recipe
Bread --o BlackBreadBake
Bread --o WhiteBreadBake
HandmadeBread --o DoughMixer
RecipeMemento --o RecipeHistory
DoughMixer --o BreadMakerMachine
Director --> RecipeHistory

@enduml
```